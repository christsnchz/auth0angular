import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './componentes/home/home.component';
import { PreciosComponent } from './componentes/precios/precios.component';
import { ProtegidaComponent } from './componentes/protegida/protegida.component';
import { CallbackComponent } from './componentes/callback/callback.component';
import { AuthGuard } from './servicios/auth.guard';


const routes: Routes = [

  {path: 'home', component: HomeComponent},
  {path: 'precios', component: PreciosComponent},
  {path: 'protegida',
   component: ProtegidaComponent,
   canActivate: [ AuthGuard ]
  },
  /* {path: 'callback', component: CallbackComponent}, */
  {path: '**', pathMatch:'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
